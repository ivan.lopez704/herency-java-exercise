package com.itswa;

public class Principal {
    public static void main(String[] args) {
        Electrodomestico electrodomestico= new Electrodomestico(10D,100D);
        Electrodomestico electrodomestico2= new Electrodomestico(10D, Electrodomestico.Colores.AZUL, Electrodomestico.Letras.A,100D);
        System.out.println("Precio Electrodomestico: " + electrodomestico.precioFinal());
        System.out.println("Precio Electrodomestico2: " + electrodomestico2.precioFinal());
        Lavadora lavadora = new Lavadora(10D,100D);
        System.out.println("Precio Lavadora: " + lavadora.precioFinal());
        Lavadora lavadora2= new Lavadora(10D, Electrodomestico.Colores.AZUL, Electrodomestico.Letras.A,100D,50D);
        System.out.println("Precio Lavadora2: " + lavadora2.precioFinal());
        Television television = new Television(10D,100D);
        Television television2= new Television(10D, Electrodomestico.Colores.AZUL, Electrodomestico.Letras.A,100D,100,true);
        System.out.println("Precion Television: "+ television.precioFinal());
        System.out.println("Precion Television2: "+ television2.precioFinal());
    }
}
