package com.itswa;

public class Electrodomestico {
    //En lugar de verificacion de los datos recibidos sean correctos, se uso enum para no tener errores.
    enum Colores{BLANCO, NEGRO, ROJO, AZUL, GRIS}
    enum Letras{A,B,C,D,F}

    protected static final Colores COLOR_DEFAULT =  Colores.BLANCO;
    protected static final Letras CONSUMO_ENERGETICO_DEFAULT = Letras.F;
    protected static final Double PRECIO_BASE_DEFAULT = 100D;
    protected static final Double PESO_DEFAULT = 5D;

    private Double precioBase;
    private Colores color;
    private Letras consumoEnergetico;
    private Double peso;

    public Electrodomestico() {
    }
    public Electrodomestico(Double precioBase, Double peso) {
        this(precioBase,COLOR_DEFAULT,CONSUMO_ENERGETICO_DEFAULT, peso);

    }

    public Electrodomestico(Double precioBase, Colores color, Letras consumoEnergetico, Double peso) {
        this.precioBase = precioBase;
        this.color = color;
        this.consumoEnergetico = consumoEnergetico;
        this.peso = peso;
    }

    public Double getPrecioBase() {
        return precioBase;
    }

    public Colores getColor() {
        return color;
    }

    public Letras getConsumoEnergetico() {
        return consumoEnergetico;
    }

    public Double getPeso() {
        return peso;
    }

    public Double precioFinal(){
        Double precioFinal= this.precioBase;
        switch (this.consumoEnergetico){
            case A -> precioFinal += 100;
            case B -> precioFinal += 80;
            case C -> precioFinal += 50;
            case D -> precioFinal += 30;
            case F -> precioFinal += 10;
        }
        if (peso <= 19D)
            precioFinal += 10D;
        else if(peso <= 49D)
            precioFinal += 50D;
        else if(peso <= 79D)
            precioFinal += 80D;
        else
            precioFinal += 100D;
        return precioFinal;
    }
}
