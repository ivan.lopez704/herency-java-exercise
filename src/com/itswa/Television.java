package com.itswa;

public class Television extends  Electrodomestico{
    protected static final int RESOLUCION_DEFAULT = 20;
    protected static final boolean SINTONIZADOR_DEFAULT = false;

    private int resolucion;
    private boolean sintonizadorTDT;

    public Television(int resolucion, boolean sintonizadorTDT) {
       super();
    }

    public Television(Double precioBase, Double peso) {
        this(precioBase,COLOR_DEFAULT,CONSUMO_ENERGETICO_DEFAULT, peso,RESOLUCION_DEFAULT, SINTONIZADOR_DEFAULT);
    }

    public Television(Double precioBase, Colores color, Letras consumoEnergetico, Double peso, int resolucion, boolean sintonizadorTDT) {
        super(precioBase, color, consumoEnergetico, peso);
        this.resolucion = resolucion;
        this.sintonizadorTDT = sintonizadorTDT;
    }

    public int getResolucion() {
        return resolucion;
    }

    public boolean isSintonizadorTDT() {
        return sintonizadorTDT;
    }

    @Override
    public Double precioFinal() {
        Double precioTelevision = super.precioFinal();
        if (this.sintonizadorTDT)
            precioTelevision+=50D;
        if (this.resolucion>40)
            precioTelevision *= 1.3D;
        return precioTelevision;
    }
}
