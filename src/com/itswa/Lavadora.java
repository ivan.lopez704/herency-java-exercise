package com.itswa;

public class Lavadora extends  Electrodomestico{
    protected static final Double CARGA_DEFAULT = 5D;
    private Double carga;

    public Lavadora() {
    }

    public Lavadora(Double precioBase, Double peso) {
        this(precioBase,COLOR_DEFAULT,CONSUMO_ENERGETICO_DEFAULT, peso,CARGA_DEFAULT);
    }

    public Lavadora(Double precioBase, Colores color, Letras consumoEnergetico, Double peso, Double carga) {
        super(precioBase, color, consumoEnergetico, peso);
        this.carga = carga;
    }

    public Double getCarga() {
        return carga;
    }

    @Override
    public Double precioFinal() {
        Double precioFinalLavadora = super.precioFinal();
        if(this.carga> 30D)
            precioFinalLavadora += 50D;
        return precioFinalLavadora;
    }
}
